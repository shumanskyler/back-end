import csv, requests, config, psycopg2
import pandas as pd
from pgcopy import CopyManager
import time
from tqdm import tqdm

apikey = config.APIKEY

def fetch_stock_data(symbol, month):
    """Fetches historical intraday data for one ticker symbol (1-min interval)

    Args:
        symbol (string): ticker symbol

    Returns:
        candlestick data (list of tuples)
    """
    interval = '1min'

    # the API requires you to slice up your requests (per month)
    # like "year1month1", "year1month2", ..., "year2month1" etc...
    slice = "year1month" + str(month) if month <= 12 else "year2month1" + str(month)

    apikey = config.APIKEY

    # formulate the correct API endpoint with symbol, slice, interval and apikey
    CSV_URL = 'https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY_EXTENDED&' \
              'symbol={symbol}&interval={interval}&slice={slice}&apikey={apikey}' \
              .format(symbol=symbol, slice=slice, interval=interval,apikey=apikey)
    
    # read CSV file directly into a pandas dataframe
    df = pd.read_csv(CSV_URL)

    # add a new symbol column to the dataframe
    # this is needed as the API doesn't return the symbol value
    df['symbol'] = symbol

    # rename columns to match database schema
    df = df.rename(columns={'time': 'time', 
                            'open': 'price_open', 
                            'high': 'price_high',
                            'low': 'price_low',
                            'close': 'price_close', 
                            'volume': 'trading_volume'})

    # convert the time column to datetime object
    # this is needed so we can seamlessly insert the data into the database later
    df['time'] = pd.to_datetime(df['time'], format='%Y-%m-%d %H:%M:%S')


    # convert the dataframe into a list of tuples ready to be ingested
    return [row for row in df.itertuples(index=False, name=None)]

def fetch_symbols() -> []:

    with open('symbols.csv') as f:
        reader = csv.reader(f)
        symbols = [row[0] for row in reader]

    return symbols

# establish database connection
conn = psycopg2.connect(database=config.DB_NAME, 
                        host=config.DB_HOST, 
                        user=config.DB_USER, 
                        password=config.DB_PASS, 
                        port=config.DB_PORT)

# column names in the database (pgcopy needs it as a parameter)
COLUMNS = ('time', 'price_open', 'price_high', 
           'price_low', 'price_close', 'trading_volume', 'symbol')

# fetch stock symbols
symbols = fetch_symbols()

# iterate over the symbols list
for symbol in tqdm(symbols):

    # specify a time range (max 24 months)
    time_range = range(1, 2) # (last 1 months)

    # iterate over the specified time range
    for month in time_range:
        print("Working on month:" + str(month))

        # fetch stock data for the given symbol and month
        # using the function you created before
        stock_data = fetch_stock_data(symbol, month)

        # create a copy manager instance
        mgr = CopyManager(conn, 'stocks_intraday', COLUMNS)

        # insert data and commit transaction
        mgr.copy(stock_data)
        conn.commit()
    time.sleep(12)
