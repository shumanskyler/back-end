import csv
import config
import requests

apikey = config.APIKEY

def fetch_symbols():

    CSV_URL = 'https://www.alphavantage.co/query?function=LISTING_STATUS&apikey={apikey}'.format(apikey=apikey)

    with requests.Session() as s:
        download = s.get(CSV_URL)
        decoded_content = download.content.decode('utf-8')
        cr = csv.reader(decoded_content.splitlines(), delimiter=',')
        my_list = list(cr)
        count = 0
        for row in my_list:
            count += 1
    return count

if __name__ == "__main__":
    print(fetch_symbols())